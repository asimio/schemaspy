FROM openjdk:8u275-jre
MAINTAINER Orlando L Otero <orlando.otero@asimio.net>, https://bitbucket.org:asimio/schemaspy.git

# Manually build using command:
# docker build -t asimio/schemaspy:latest .

# Example:
# docker run --net=host -e DB_TYPE=pgsql -e DB_HOST=`hostname` -e DB_PORT=5432 -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit -e DB_SCHEMAS=public -e JDBC_DRIVER_PATH=/opt/asimio/schemaspy/jdbc -v ~/Downloads/schemaspy:/opt/asimio/schemaspy asimio/schemaspy:latest

VOLUME /tmp

RUN \
  bash -c 'mkdir -p /opt/asimio/schemaspy/output' && \
  apt-get -qq update && \
  apt-get -y upgrade && apt-get -y autoclean && apt-get -y autoremove && \
  bash -c 'DEBIAN_FRONTEND=noninteractive apt-get install -qq -y graphviz' && \
  wget --output-document=/opt/asimio/schemaspy.jar https://github.com/schemaspy/schemaspy/releases/download/v6.1.0/schemaspy-6.1.0.jar && \
  rm -rf /var/lib/apt/lists/*

CMD \
  java -jar /opt/asimio/schemaspy.jar -t $DB_TYPE -host $DB_HOST -port $DB_PORT -db $DB_NAME -u $DB_USER -p $DB_PASSWD -schemas $DB_SCHEMAS -dp $JDBC_DRIVER_PATH -o /opt/asimio/schemaspy/output/$DB_NAME
