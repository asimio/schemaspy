# README #

A Docker image to document relational databases including the ER diagram.

Accompanying source code for blog entry at https://tech.asimio.net/2020/11/27/Documenting-your-relational-database-using-SchemaSpy.html

### Build

```bash
docker build -t asimio/schemaspy:latest .
```

### Usage example:

See https://bitbucket.org/asimio/postgres/src/master/db_dvdrental/ to learn how to run a Docker container with a Postgres DB, `db_dvdrental`.

```bash
docker run --net=host -e DB_TYPE=pgsql -e DB_HOST=`hostname` -e DB_PORT=5432 -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit -e DB_SCHEMAS=public -e JDBC_DRIVER_PATH=/opt/asimio/schemaspy/jdbc -v ~/Downloads/schemaspy:/opt/asimio/schemaspy asimio/schemaspy:latest
```

### Expected documentation output

`~/Downloads/schemaspy/output/db_dvdrental/public/index.html`


### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero